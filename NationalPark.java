import java.util.Scanner;
public class NationalPark
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		cuteCat[] clowder = new cuteCat[4];
		
		for (int i = 0; i<clowder.length; i++)
			{
				System.out.println("Enter the weight (in kg), name of the breed, and the longevity (in years) of cat " + (i+1));
				clowder[i] = new cuteCat(Double.parseDouble(scan.nextLine()), scan.nextLine(), Integer.parseInt(scan.nextLine()));
				// System.out.println("How much does the average cat of this breed weight? (in kg)");
				// clowder[i].setWeight(Double.parseDouble(scan.nextLine()));
				// System.out.println("What is the name of this breed? ");
				// clowder[i].setName(scan.nextLine());
				// System.out.println("Up to how many years can it live?");
				// clowder[i].setLongevity(Integer.parseInt(scan.nextLine()));
			}
		
		System.out.println("--------------------------------------------------");
		
		System.out.println("Weight of the cat: " + clowder[clowder.length-1].getWeight() + " kg");
		System.out.println("Name of the cat: " + clowder[clowder.length-1].getName() + " cat");
		System.out.println("Longevity of the cat: " + clowder[clowder.length-1].getLongevity() + " years");

		clowder[0].sayHi();
		clowder[0].iLive();
		
		System.out.println("--------------------------------------------------");
		
		System.out.println("How many fish did your chonky cat eat?");
		int fish = scan.nextInt();
		
		clowder[1].iWeight(fish);
		
		System.out.println("--------------------------------------------------");
		
		System.out.println("Fields of the last animal BEFORE the call of the setters");
		System.out.println("Weight: " + clowder[clowder.length-1].getWeight());
		System.out.println("Name: " + clowder[clowder.length-1].getName() + " cat");
		System.out.println("Longevity: " + clowder[clowder.length-1].getLongevity());
		
		System.out.println("What is the updated weight of your last cat?");
		clowder[clowder.length-1].setWeight(scan.nextDouble());
		//System.out.println("What is the updated name of your last cat?");
		//clowder[clowder.length-1].setName(scan.next());
		System.out.println("What is the updated longevity of your last cat?");
		clowder[clowder.length-1].setLongevity(scan.nextInt());
		
		System.out.println("Fields of the last animal AFTER the call of the setters");
		System.out.println("Weight: " + clowder[clowder.length-1].getWeight());
		System.out.println("Name: " + clowder[clowder.length-1].getName() + " cat");
		System.out.println("Longevity: " + clowder[clowder.length-1].getLongevity());
		
		
		scan.close();
	}
	
	
}