public class cuteCat
{
	private double weight;
	private String name;
	private int longevity;


//getter methods
	public double getWeight ()
	{
		return this.weight;
	}
	
	public String getName ()
	{
		return this.name;
	}
	
	public int getLongevity ()
	{
		return this.longevity;
	}
	
	
//setter methods
	public void setWeight(double newWeight)
	{
		this.weight = newWeight;
	}
	
	//public void setName(String newName)
	//{
	//	this.name = newName;
	//}
	
	public void setLongevity(int newLongevity)
	{
		this.longevity = newLongevity;
	}


	//constructor methods
	public cuteCat(double otherWeight, String otherName, int otherLongevity)
	{
		this.weight = otherWeight;
		this.name = otherName;
		this.longevity = otherLongevity;
	}

	public void sayHi()
	{
		System.out.println("I am a " + name + "!");
	}
	
	public void iLive()
	{
		System.out.println("I can live up to " + longevity + " year(s)!");
	}
	
	
	
	
	public void iWeight(int amountOfFish)
	{
		double weight = 0;
		if (fishValidator(amountOfFish))
		{
			//average fish weight = 4 pounds = 1.8kg
			weight = this.weight + amountOfFish*1.8;
			System.out.println("Your cat now weights " + weight + " kg.");
		}
		
		else
		{
			System.out.println("It can't eat that much!! The cat is going to explode!");
		}
		
	}
	
	private boolean fishValidator (int amountOfFish)
	{
		boolean isFishValid = amountOfFish>=0 && amountOfFish<4;
		return isFishValid;
	}
}